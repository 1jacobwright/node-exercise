const { peopleService } = require('../services/peopleService')
const router = require('express').Router();

const getPeople = async (req, res, next) => {
  try {
    let allPeople = await peopleService.getAllPeople(req.query.sortBy)
    return res.send(allPeople)
    next()
  } catch(e) {
    console.log(e.message)
    res.sendStatus(500) && next(error)
  }
}

module.exports = { router, getPeople };