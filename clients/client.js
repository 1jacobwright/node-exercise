const request = require('request')

const doGet = async (url) => {
  try {
    let response = request(url, (error, response, body) => {
       let jsonBody;
       if (!error && response.statusCode == 200) {
         let jsonBody = JSON.parse(body) 
         return resolve(jsonBody)
       }
     })
    return response
  } catch(e) {
    throw new Error(e.message)
  }
}

module.exports = { doGet };