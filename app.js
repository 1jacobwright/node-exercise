const express = require('express')
const app = express()
const port = process.env.PORT || 3000
var routes = require('./routes/route')

app.listen(port, () => {
  console.log(`Starting StarWars api`)
})

app.use(routes);

module.exports = { app };