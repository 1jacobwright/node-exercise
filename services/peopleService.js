const { client } = require('../clients/client')

const getAllPeople = async (sortOption) => {
  try {
    console.log('peopleService.getAllPeople called')
    let allPeople = await getStarWarsPeople() 
    if (sortOption != null && ['name', 'mass', 'height'].includes(sortOption)) {
      allPeople.sort((p1, p2) => {
        return compare(sortOption, p1, p2)
      })
    }
    return res.send(allPeople)
  } catch(e) {
    throw new Error(e.message)
  }
}

async function getStarWarsPeople() {
  //todo - url into an index.js file that looks at env variables
  url = 'http://swapi.dev/api/people'
  let allPeople = []
  do {
    let people = await client.doGet(url)
    url = people.next
    allPeople.push.apply(allPeople, people.results)
  }
  while (url !== null)

  return allPeople;
}

function compare(sortOption, p1, p2) {
  if (sortOption == 'mass') {
     let p1Mass = p1.mass != 'unknown' ? p1.mass.split(',').join('') : 1000000
     let p2Mass = p2.mass != 'unknown' ? p2.mass.split(',').join('') : 1000000
     return parseInt(p1Mass) - parseInt(p2Mass)
  } else if (sortOption == 'name') {
     return (p1.name < p2.name) ? -1 : (p1.name > p2.name) ? 1 : 0;
  } else if (sortOption == 'height') {
    let p1Height = p1.height != 'unknown' ? p1.height : 1000000
    let p2Height = p2.height != 'unknown' ? p2.height : 1000000
    return parseInt(p1Height) - parseInt(p2Height)
  }
  return 0
}

module.exports = { getAllPeople };