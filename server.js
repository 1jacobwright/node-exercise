const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const asyncHandler = require('express-async-handler')
const request = require('request')

app.get('/people', asyncHandler(async (req, res) => {
   let allPeople = req.query.optimized === '' ? await getAllPeopleOptimized() : await getAllPeople()
   let sortOption = req.query.sortBy
   if (sortOption != null && ['name', 'mass', 'height'].includes(sortOption)) {
      allPeople.sort((p1, p2) => {
        return compare(sortOption, p1, p2)
      })
   }
   return res.send(allPeople)
}))

app.get('/planets', asyncHandler(async (req, res) => {
   let allPlanets = await getAllPlanets()
   return res.send(allPlanets)
}))

app.listen(port, () => {
  console.log(`Starting StarWars api`)
})

async function getAllPlanets() {
  url = 'http://swapi.dev/api/planets'
  let allPlanets = []
  do {
    let planets = await doGet(url)
    url = planets.next
    planets.results.forEach((planet, index) => {
       let residents = []
       planet.residents.forEach(async (resident) => {
           let person = await doGet(resident)
           residents.push(person.name)
       })
       planets.results[index].residents = residents
    })
    allPlanets.push.apply(allPlanets, planets.results)
  }
  while (url !== null)

  return allPlanets;
}      

function compare(sortOption, p1, p2) {
  if (sortOption == 'mass') {
     let p1Mass = p1.mass != 'unknown' ? p1.mass.split(',').join('') : 1000000
     let p2Mass = p2.mass != 'unknown' ? p2.mass.split(',').join('') : 1000000
     return parseInt(p1Mass) - parseInt(p2Mass)
  } else if (sortOption == 'name') {
     return (p1.name < p2.name) ? -1 : (p1.name > p2.name) ? 1 : 0;
  } else if (sortOption == 'height') {
    let p1Height = p1.height != 'unknown' ? p1.height : 1000000
    let p2Height = p2.height != 'unknown' ? p2.height : 1000000
    return parseInt(p1Height) - parseInt(p2Height)
  }
  return 0
}

async function getAllPeople() {
  url = 'http://swapi.dev/api/people'
  let allPeople = []
  do {
    let people = await doGet(url)
    url = people.next
    allPeople.push.apply(allPeople, people.results)
  }
  while (url !== null)

  return allPeople;
}

async function getAllPeopleOptimized() {
  let allPeople = []
  url = 'http://swapi.dev/api/people'
  let fPeople = await doGet(url)
  allPeople.push.apply(allPeople, fPeople.results)
  let lastPage = Math.ceil(fPeople.count / fPeople.results.length)
  let promises = []
  if (lastPage > 1) {
    let i
    for (i = 2; i <= lastPage; i++) {
      promises.push(doGet(url + '?page=' + i))  
    }
    let pagedPeople = await Promise.all(promises)
    pagedPeople.forEach(async (people) => {
      allPeople.push.apply(allPeople, people.results)
    })
  }  
  return allPeople
}

function doGet(url) {
  return new Promise((resolve, reject) => { 
    request(url, (error, response, body) => {
      let jsonBody;
      if (!error && response.statusCode == 200) {
        let jsonBody = JSON.parse(body) 
        return resolve(jsonBody)
      } else {
        reject('failed to call url')
      }
    })
  })
}
