const people = require('../controllers/people')
const router = require('express').Router();

router.get('/people', people.getPeople)

//router.get('/planets', planets.getPlanets)

module.exports = { router };